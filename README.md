# Signavio Backend Coding Challenge
Thank you very much for taking an interest in working at Signavio! After your first interview with us, we'd like to get to know your approach to problem solving with this coding challenge.

This challenge is actually closely related to the requirements of one of our products: Process Intelligence. This product enables Signavio customers to analyse big data generated from their processes and lets them recognize the gaps and variants from the intended business process (to-be) and the operating technical processes (as-is). Find out more about Process Intelligence [here](https://www.signavio.com/products/process-intelligence/)!

## The Goal
Please solve the challenge below by meeting the given acceptance criteria. Push your solution to this private GitHub repository and please do not publish your solution in any public GitHub repository or anywhere else!

Should you have any questions regarding the challenge, please email: coding.challenge.backend@signavio.com

In general, feel free to make assumptions and have fun with the challenge!
Please make sure you focus your time on explaining and elaborating your thought process, assumptions, decisions, thoughts and comments, for example inside a `thought-process.md` file. Please also document how to build and run your code.

## Tech Stack
* Java or Kotlin
* Gradle or Maven
* No external databases, do all calculations in memory to avoid complexity
* Test code where you feel it makes sense

## Details
A Signavio customer, a multinational company that builds industrial appliances, has an internal system dedicated to procuring (buying) any and all resources the company requires to operate. Procurement is done via the company's own ERP (Enterprise Resource Planning) system.

A typical business process represented the ERP system is "procure-to-pay", which generally involves the following activities:
* create purchase request
* request approved
* create purchase order
* select supplier
* receive goods
* pay invoice

Whenever the company wants to buy something, they do so through their ERP system.

The company buys many resources, always using their ERP system. Each resource purchase can be considered a case, or single instance of this process. As it happens, the actual as-is process often deviates from the ideal to-be process. Sometimes purchase requests are raised but never get approved, sometimes a supplier is selected but the goods are never received, sometimes it simply takes a long time to complete the process, and so on. We call these deviations from the process path variants.

The customer provides us with extracted process data from their existing ERP (Enterprise Resource Planning) system. The customer extracted one of their processes for analysis: Procure to Pay. The logfiles contain 3 columns:
* activity name
* case id
* timestamp

We want to analyse and compare process instances (cases) with each other. You can find the sample data set [here](samples/Activity_Log_2004_to_2014.csv).

## Acceptance Criteria
* Aggregate cases that have the same event execution order and list the 10 variants with the most cases.
* Provide your output as JSON, choose a structure that makes sense.
* As that output is used by other highly interactive components, we need to be able to get the query results in well under 50 milliseconds.

## Example
![Variants example](images/example.png)
